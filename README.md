# My zerogravity project

... the best thing since swiss chees.

## Abstract

Zerogravity is a python3 polygon mesh to game render library
using dokerized [blender](https://www.blender.org/) 3D creation software
as backend.

Minimal requirement:

+ Language: python 3.6
+ Library dependencies: docker >= 2.0, requests, setuptools
+ Software dependencies: [Docker](https://www.docker.com/)
+ License: >= GPLv3
+ Date origin: 2017-05
+ Programmer: bue
+ Source code: https://gitlab.com/biotransistor/zerogravity
+ User manual: https://zerogravity.readthedocs.io/en/latest/


## User manual

The [user manual]( https://zerogravity.readthedocs.io/en/latest/)
is hosted at **Read the Docs**.
