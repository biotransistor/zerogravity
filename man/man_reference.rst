  .. bue 2018-04-10: https://gisellezeno.com/tutorials/sphinx-for-python-documentation.html 
  .. built by sphinx-apidoc -f -o . ../zerogravity/

Refefrence
==========

Submodules
----------

  .. bue 2018-09-02: I have to write some  general idea about the zerogravity stack.
  .. how things work in the background!
  .. The docker file


  .. zerogravity.bgectrlrmodule module
  .. ---------------------------------
  .. bue 2018-04-0: module not imported zerogravity.bgectrlrmodule
  .. this is blender python3 controller code
  .. import bge works only insied python
  .. maybe i should write some word about this and how such code is developed

  .. have a look at "Howto develop new zerogravitymodules" for a more practikel
     understanding how zero gravity works.


zerogravity.zerog module
------------------------

.. automodule:: zerogravity.zerog
    :members:
    :undoc-members:
    :show-inheritance:

zerogravity.zerogmodule module
------------------------------

.. automodule:: zerogravity.zerogmodule
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: zerogravity
    :members:
    :undoc-members:
    :show-inheritance:
