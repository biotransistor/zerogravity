  .. bue 2018-04-10: https://docs.readthedocs.io/en/latest/getting_started.html
  .. danielle procida, what nobody tells about documentation: https://www.youtube.com/watch?v=azf6yzuJt54

.. zeroGravity documentation master file, created by
   sphinx-quickstart on Mon Apr  9 20:08:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to zeroGravity's Documentation.
=======================================

.. image:: img/sallyRide.jpg
           :height: 384
           :alt: zeroG sally ride
           :align: center

*A python3 polygon mesh to game render library using dokerized blender \
3D software as backend.* (Photo: Sally Ride at zero G; photo owned by NASA)

**Summary:**
`zeroGravity`_ is

.. _zeroGravity: https://gitlab.com/biotransistor/zerogravity

**Implementation:**
3 2 1 liftoff

**Requirements**
Python >= 3.6
Docker
Platform: Linux, Mac or Windows

**license:**
Source code is available under `GNU AGPLv3`_ license.
This manual is written under `GNU FDLv1.3`_ license.

.. _GNU AGPLv3: https://www.gnu.org/licenses/licenses.html#AGPL
.. _GNU FDLv1.3: https://www.gnu.org/licenses/licenses.html#FDL

Copyright (C)  2017,2018  Elmar Bucher.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   man_tutorial
   man_howto
   man_reference
   man_discussion
   fdl-1.3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
